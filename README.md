migrated to -> https://bitbucket.org/Tiendas/tienda-back
# README #

Este es el repositorio del back-end. Por ahora estoy usando el gem Devise para el registro y autenticacion de usarios. Al principio la vista estara en rails pero despues intentare pasarla a angular.

### What is this repository for? ###

* repositorio del backend Back-end
* 1.0


### How do I get set up? ###

* bundle install
* rake db:create
* rake db:migrate
* rails s